package com.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.myapp.IntegrationTest;
import com.myapp.domain.TestCase;
import com.myapp.domain.UserStory;
import com.myapp.repository.EntityManager;
import com.myapp.repository.TestCaseRepository;
import com.myapp.service.TestCaseService;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link TestCaseResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class TestCaseResourceIT {

    private static final Integer DEFAULT_CASE_ID = 1;
    private static final Integer UPDATED_CASE_ID = 2;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/test-cases";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TestCaseRepository testCaseRepository;

    @Mock
    private TestCaseRepository testCaseRepositoryMock;

    @Mock
    private TestCaseService testCaseServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private TestCase testCase;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestCase createEntity(EntityManager em) {
        TestCase testCase = new TestCase().caseID(DEFAULT_CASE_ID).name(DEFAULT_NAME).summary(DEFAULT_SUMMARY).status(DEFAULT_STATUS);
        // Add required entity
        UserStory userStory;
        userStory = em.insert(UserStoryResourceIT.createEntity(em)).block();
        testCase.setTestcase(userStory);
        return testCase;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TestCase createUpdatedEntity(EntityManager em) {
        TestCase testCase = new TestCase().caseID(UPDATED_CASE_ID).name(UPDATED_NAME).summary(UPDATED_SUMMARY).status(UPDATED_STATUS);
        // Add required entity
        UserStory userStory;
        userStory = em.insert(UserStoryResourceIT.createUpdatedEntity(em)).block();
        testCase.setTestcase(userStory);
        return testCase;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(TestCase.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
        UserStoryResourceIT.deleteEntities(em);
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        testCase = createEntity(em);
    }

    @Test
    void createTestCase() throws Exception {
        int databaseSizeBeforeCreate = testCaseRepository.findAll().collectList().block().size();
        // Create the TestCase
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeCreate + 1);
        TestCase testTestCase = testCaseList.get(testCaseList.size() - 1);
        assertThat(testTestCase.getCaseID()).isEqualTo(DEFAULT_CASE_ID);
        assertThat(testTestCase.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTestCase.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testTestCase.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void createTestCaseWithExistingId() throws Exception {
        // Create the TestCase with an existing ID
        testCase.setId(1L);

        int databaseSizeBeforeCreate = testCaseRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkCaseIDIsRequired() throws Exception {
        int databaseSizeBeforeTest = testCaseRepository.findAll().collectList().block().size();
        // set the field null
        testCase.setCaseID(null);

        // Create the TestCase, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = testCaseRepository.findAll().collectList().block().size();
        // set the field null
        testCase.setName(null);

        // Create the TestCase, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = testCaseRepository.findAll().collectList().block().size();
        // set the field null
        testCase.setStatus(null);

        // Create the TestCase, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllTestCases() {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        // Get all the testCaseList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(testCase.getId().intValue()))
            .jsonPath("$.[*].caseID")
            .value(hasItem(DEFAULT_CASE_ID))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME))
            .jsonPath("$.[*].summary")
            .value(hasItem(DEFAULT_SUMMARY))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTestCasesWithEagerRelationshipsIsEnabled() {
        when(testCaseServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(testCaseServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllTestCasesWithEagerRelationshipsIsNotEnabled() {
        when(testCaseServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(testCaseServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getTestCase() {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        // Get the testCase
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, testCase.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(testCase.getId().intValue()))
            .jsonPath("$.caseID")
            .value(is(DEFAULT_CASE_ID))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME))
            .jsonPath("$.summary")
            .value(is(DEFAULT_SUMMARY))
            .jsonPath("$.status")
            .value(is(DEFAULT_STATUS));
    }

    @Test
    void getNonExistingTestCase() {
        // Get the testCase
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewTestCase() throws Exception {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();

        // Update the testCase
        TestCase updatedTestCase = testCaseRepository.findById(testCase.getId()).block();
        updatedTestCase.caseID(UPDATED_CASE_ID).name(UPDATED_NAME).summary(UPDATED_SUMMARY).status(UPDATED_STATUS);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedTestCase.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedTestCase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
        TestCase testTestCase = testCaseList.get(testCaseList.size() - 1);
        assertThat(testTestCase.getCaseID()).isEqualTo(UPDATED_CASE_ID);
        assertThat(testTestCase.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTestCase.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testTestCase.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void putNonExistingTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, testCase.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateTestCaseWithPatch() throws Exception {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();

        // Update the testCase using partial update
        TestCase partialUpdatedTestCase = new TestCase();
        partialUpdatedTestCase.setId(testCase.getId());

        partialUpdatedTestCase.caseID(UPDATED_CASE_ID).name(UPDATED_NAME);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTestCase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTestCase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
        TestCase testTestCase = testCaseList.get(testCaseList.size() - 1);
        assertThat(testTestCase.getCaseID()).isEqualTo(UPDATED_CASE_ID);
        assertThat(testTestCase.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTestCase.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testTestCase.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    void fullUpdateTestCaseWithPatch() throws Exception {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();

        // Update the testCase using partial update
        TestCase partialUpdatedTestCase = new TestCase();
        partialUpdatedTestCase.setId(testCase.getId());

        partialUpdatedTestCase.caseID(UPDATED_CASE_ID).name(UPDATED_NAME).summary(UPDATED_SUMMARY).status(UPDATED_STATUS);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedTestCase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedTestCase))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
        TestCase testTestCase = testCaseList.get(testCaseList.size() - 1);
        assertThat(testTestCase.getCaseID()).isEqualTo(UPDATED_CASE_ID);
        assertThat(testTestCase.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTestCase.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testTestCase.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    void patchNonExistingTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, testCase.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamTestCase() throws Exception {
        int databaseSizeBeforeUpdate = testCaseRepository.findAll().collectList().block().size();
        testCase.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(testCase))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the TestCase in the database
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteTestCase() {
        // Initialize the database
        testCaseRepository.save(testCase).block();

        int databaseSizeBeforeDelete = testCaseRepository.findAll().collectList().block().size();

        // Delete the testCase
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, testCase.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<TestCase> testCaseList = testCaseRepository.findAll().collectList().block();
        assertThat(testCaseList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
