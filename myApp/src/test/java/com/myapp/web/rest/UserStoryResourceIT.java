package com.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.myapp.IntegrationTest;
import com.myapp.domain.UserStory;
import com.myapp.repository.EntityManager;
import com.myapp.repository.UserStoryRepository;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link UserStoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class UserStoryResourceIT {

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_CASE_ID = 1;
    private static final Integer UPDATED_CASE_ID = 2;

    private static final String ENTITY_API_URL = "/api/user-stories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserStoryRepository userStoryRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private UserStory userStory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserStory createEntity(EntityManager em) {
        UserStory userStory = new UserStory().status(DEFAULT_STATUS).description(DEFAULT_DESCRIPTION).caseID(DEFAULT_CASE_ID);
        return userStory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserStory createUpdatedEntity(EntityManager em) {
        UserStory userStory = new UserStory().status(UPDATED_STATUS).description(UPDATED_DESCRIPTION).caseID(UPDATED_CASE_ID);
        return userStory;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(UserStory.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        userStory = createEntity(em);
    }

    @Test
    void createUserStory() throws Exception {
        int databaseSizeBeforeCreate = userStoryRepository.findAll().collectList().block().size();
        // Create the UserStory
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeCreate + 1);
        UserStory testUserStory = userStoryList.get(userStoryList.size() - 1);
        assertThat(testUserStory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserStory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testUserStory.getCaseID()).isEqualTo(DEFAULT_CASE_ID);
    }

    @Test
    void createUserStoryWithExistingId() throws Exception {
        // Create the UserStory with an existing ID
        userStory.setId(1L);

        int databaseSizeBeforeCreate = userStoryRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = userStoryRepository.findAll().collectList().block().size();
        // set the field null
        userStory.setStatus(null);

        // Create the UserStory, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = userStoryRepository.findAll().collectList().block().size();
        // set the field null
        userStory.setDescription(null);

        // Create the UserStory, which fails.

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllUserStories() {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        // Get all the userStoryList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(userStory.getId().intValue()))
            .jsonPath("$.[*].status")
            .value(hasItem(DEFAULT_STATUS))
            .jsonPath("$.[*].description")
            .value(hasItem(DEFAULT_DESCRIPTION))
            .jsonPath("$.[*].caseID")
            .value(hasItem(DEFAULT_CASE_ID));
    }

    @Test
    void getUserStory() {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        // Get the userStory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, userStory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(userStory.getId().intValue()))
            .jsonPath("$.status")
            .value(is(DEFAULT_STATUS))
            .jsonPath("$.description")
            .value(is(DEFAULT_DESCRIPTION))
            .jsonPath("$.caseID")
            .value(is(DEFAULT_CASE_ID));
    }

    @Test
    void getNonExistingUserStory() {
        // Get the userStory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewUserStory() throws Exception {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();

        // Update the userStory
        UserStory updatedUserStory = userStoryRepository.findById(userStory.getId()).block();
        updatedUserStory.status(UPDATED_STATUS).description(UPDATED_DESCRIPTION).caseID(UPDATED_CASE_ID);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedUserStory.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedUserStory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
        UserStory testUserStory = userStoryList.get(userStoryList.size() - 1);
        assertThat(testUserStory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserStory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testUserStory.getCaseID()).isEqualTo(UPDATED_CASE_ID);
    }

    @Test
    void putNonExistingUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, userStory.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateUserStoryWithPatch() throws Exception {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();

        // Update the userStory using partial update
        UserStory partialUpdatedUserStory = new UserStory();
        partialUpdatedUserStory.setId(userStory.getId());

        partialUpdatedUserStory.description(UPDATED_DESCRIPTION);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedUserStory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedUserStory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
        UserStory testUserStory = userStoryList.get(userStoryList.size() - 1);
        assertThat(testUserStory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserStory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testUserStory.getCaseID()).isEqualTo(DEFAULT_CASE_ID);
    }

    @Test
    void fullUpdateUserStoryWithPatch() throws Exception {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();

        // Update the userStory using partial update
        UserStory partialUpdatedUserStory = new UserStory();
        partialUpdatedUserStory.setId(userStory.getId());

        partialUpdatedUserStory.status(UPDATED_STATUS).description(UPDATED_DESCRIPTION).caseID(UPDATED_CASE_ID);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedUserStory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedUserStory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
        UserStory testUserStory = userStoryList.get(userStoryList.size() - 1);
        assertThat(testUserStory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserStory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testUserStory.getCaseID()).isEqualTo(UPDATED_CASE_ID);
    }

    @Test
    void patchNonExistingUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, userStory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamUserStory() throws Exception {
        int databaseSizeBeforeUpdate = userStoryRepository.findAll().collectList().block().size();
        userStory.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(userStory))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the UserStory in the database
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteUserStory() {
        // Initialize the database
        userStoryRepository.save(userStory).block();

        int databaseSizeBeforeDelete = userStoryRepository.findAll().collectList().block().size();

        // Delete the userStory
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, userStory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<UserStory> userStoryList = userStoryRepository.findAll().collectList().block();
        assertThat(userStoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
