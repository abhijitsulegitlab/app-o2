import { IUserStory } from 'app/shared/model/user-story.model';

export interface ITestCase {
  id?: number;
  caseID?: number;
  name?: string;
  summary?: string | null;
  status?: string;
  testcase?: IUserStory;
  userStory?: IUserStory | null;
}

export const defaultValue: Readonly<ITestCase> = {};
