import { ITestCase } from 'app/shared/model/test-case.model';

export interface IUserStory {
  id?: number;
  status?: string;
  description?: string;
  caseID?: number | null;
  userstories?: ITestCase[] | null;
}

export const defaultValue: Readonly<IUserStory> = {};
