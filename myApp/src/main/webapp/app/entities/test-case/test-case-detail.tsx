import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './test-case.reducer';

export const TestCaseDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const testCaseEntity = useAppSelector(state => state.myapp.testCase.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="testCaseDetailsHeading">
          <Translate contentKey="myApp.testCase.detail.title">TestCase</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{testCaseEntity.id}</dd>
          <dt>
            <span id="caseID">
              <Translate contentKey="myApp.testCase.caseID">Case ID</Translate>
            </span>
          </dt>
          <dd>{testCaseEntity.caseID}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="myApp.testCase.name">Name</Translate>
            </span>
          </dt>
          <dd>{testCaseEntity.name}</dd>
          <dt>
            <span id="summary">
              <Translate contentKey="myApp.testCase.summary">Summary</Translate>
            </span>
          </dt>
          <dd>{testCaseEntity.summary}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="myApp.testCase.status">Status</Translate>
            </span>
          </dt>
          <dd>{testCaseEntity.status}</dd>
          <dt>
            <Translate contentKey="myApp.testCase.testcase">Testcase</Translate>
          </dt>
          <dd>{testCaseEntity.testcase ? testCaseEntity.testcase.caseID : ''}</dd>
          <dt>
            <Translate contentKey="myApp.testCase.userStory">User Story</Translate>
          </dt>
          <dd>{testCaseEntity.userStory ? testCaseEntity.userStory.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/test-case" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/test-case/${testCaseEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default TestCaseDetail;
