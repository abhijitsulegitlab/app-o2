import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IUserStory } from 'app/shared/model/user-story.model';
import { getEntities as getUserStories } from 'app/entities/user-story/user-story.reducer';
import { ITestCase } from 'app/shared/model/test-case.model';
import { getEntity, updateEntity, createEntity, reset } from './test-case.reducer';

export const TestCaseUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const userStories = useAppSelector(state => state.myapp.userStory.entities);
  const testCaseEntity = useAppSelector(state => state.myapp.testCase.entity);
  const loading = useAppSelector(state => state.myapp.testCase.loading);
  const updating = useAppSelector(state => state.myapp.testCase.updating);
  const updateSuccess = useAppSelector(state => state.myapp.testCase.updateSuccess);
  const handleClose = () => {
    props.history.push('/test-case' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }

    dispatch(getUserStories({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...testCaseEntity,
      ...values,
      testcase: userStories.find(it => it.id.toString() === values.testcase.toString()),
      userStory: userStories.find(it => it.id.toString() === values.userStory.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...testCaseEntity,
          testcase: testCaseEntity?.testcase?.id,
          userStory: testCaseEntity?.userStory?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="myApp.testCase.home.createOrEditLabel" data-cy="TestCaseCreateUpdateHeading">
            <Translate contentKey="myApp.testCase.home.createOrEditLabel">Create or edit a TestCase</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="test-case-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('myApp.testCase.caseID')}
                id="test-case-caseID"
                name="caseID"
                data-cy="caseID"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('myApp.testCase.name')}
                id="test-case-name"
                name="name"
                data-cy="name"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                label={translate('myApp.testCase.summary')}
                id="test-case-summary"
                name="summary"
                data-cy="summary"
                type="text"
              />
              <ValidatedField
                label={translate('myApp.testCase.status')}
                id="test-case-status"
                name="status"
                data-cy="status"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                }}
              />
              <ValidatedField
                id="test-case-testcase"
                name="testcase"
                data-cy="testcase"
                label={translate('myApp.testCase.testcase')}
                type="select"
                required
              >
                <option value="" key="0" />
                {userStories
                  ? userStories.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.caseID}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <FormText>
                <Translate contentKey="entity.validation.required">This field is required.</Translate>
              </FormText>
              <ValidatedField
                id="test-case-userStory"
                name="userStory"
                data-cy="userStory"
                label={translate('myApp.testCase.userStory')}
                type="select"
              >
                <option value="" key="0" />
                {userStories
                  ? userStories.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/test-case" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default TestCaseUpdate;
