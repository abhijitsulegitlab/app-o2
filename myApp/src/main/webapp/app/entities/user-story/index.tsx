import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserStory from './user-story';
import UserStoryDetail from './user-story-detail';
import UserStoryUpdate from './user-story-update';
import UserStoryDeleteDialog from './user-story-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserStoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserStoryUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserStoryDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserStory} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={UserStoryDeleteDialog} />
  </>
);

export default Routes;
