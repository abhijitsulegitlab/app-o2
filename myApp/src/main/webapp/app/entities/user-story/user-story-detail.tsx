import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './user-story.reducer';

export const UserStoryDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const userStoryEntity = useAppSelector(state => state.myapp.userStory.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="userStoryDetailsHeading">
          <Translate contentKey="myApp.userStory.detail.title">UserStory</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{userStoryEntity.id}</dd>
          <dt>
            <span id="status">
              <Translate contentKey="myApp.userStory.status">Status</Translate>
            </span>
          </dt>
          <dd>{userStoryEntity.status}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="myApp.userStory.description">Description</Translate>
            </span>
          </dt>
          <dd>{userStoryEntity.description}</dd>
          <dt>
            <span id="caseID">
              <Translate contentKey="myApp.userStory.caseID">Case ID</Translate>
            </span>
          </dt>
          <dd>{userStoryEntity.caseID}</dd>
        </dl>
        <Button tag={Link} to="/user-story" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/user-story/${userStoryEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default UserStoryDetail;
