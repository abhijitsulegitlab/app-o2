import testCase from 'app/entities/test-case/test-case.reducer';
import userStory from 'app/entities/user-story/user-story.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  testCase,
  userStory,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
