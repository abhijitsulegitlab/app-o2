package com.myapp.repository;

import com.myapp.domain.UserStory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the UserStory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserStoryRepository extends ReactiveCrudRepository<UserStory, Long>, UserStoryRepositoryInternal {
    Flux<UserStory> findAllBy(Pageable pageable);

    @Override
    <S extends UserStory> Mono<S> save(S entity);

    @Override
    Flux<UserStory> findAll();

    @Override
    Mono<UserStory> findById(Long id);

    @Override
    Mono<Void> deleteById(Long id);
}

interface UserStoryRepositoryInternal {
    <S extends UserStory> Mono<S> save(S entity);

    Flux<UserStory> findAllBy(Pageable pageable);

    Flux<UserStory> findAll();

    Mono<UserStory> findById(Long id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<UserStory> findAllBy(Pageable pageable, Criteria criteria);

}
