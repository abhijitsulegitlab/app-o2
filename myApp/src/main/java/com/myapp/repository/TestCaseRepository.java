package com.myapp.repository;

import com.myapp.domain.TestCase;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the TestCase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TestCaseRepository extends ReactiveCrudRepository<TestCase, Long>, TestCaseRepositoryInternal {
    Flux<TestCase> findAllBy(Pageable pageable);

    @Override
    Mono<TestCase> findOneWithEagerRelationships(Long id);

    @Override
    Flux<TestCase> findAllWithEagerRelationships();

    @Override
    Flux<TestCase> findAllWithEagerRelationships(Pageable page);

    @Query("SELECT * FROM test_case entity WHERE entity.testcase_id = :id")
    Flux<TestCase> findByTestcase(Long id);

    @Query("SELECT * FROM test_case entity WHERE entity.testcase_id IS NULL")
    Flux<TestCase> findAllWhereTestcaseIsNull();

    @Query("SELECT * FROM test_case entity WHERE entity.user_story_id = :id")
    Flux<TestCase> findByUserStory(Long id);

    @Query("SELECT * FROM test_case entity WHERE entity.user_story_id IS NULL")
    Flux<TestCase> findAllWhereUserStoryIsNull();

    @Override
    <S extends TestCase> Mono<S> save(S entity);

    @Override
    Flux<TestCase> findAll();

    @Override
    Mono<TestCase> findById(Long id);

    @Override
    Mono<Void> deleteById(Long id);
}

interface TestCaseRepositoryInternal {
    <S extends TestCase> Mono<S> save(S entity);

    Flux<TestCase> findAllBy(Pageable pageable);

    Flux<TestCase> findAll();

    Mono<TestCase> findById(Long id);
    // this is not supported at the moment because of https://github.com/jhipster/generator-jhipster/issues/18269
    // Flux<TestCase> findAllBy(Pageable pageable, Criteria criteria);

    Mono<TestCase> findOneWithEagerRelationships(Long id);

    Flux<TestCase> findAllWithEagerRelationships();

    Flux<TestCase> findAllWithEagerRelationships(Pageable page);

    Mono<Void> deleteById(Long id);
}
