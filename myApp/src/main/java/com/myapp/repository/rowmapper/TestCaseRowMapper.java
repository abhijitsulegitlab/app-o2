package com.myapp.repository.rowmapper;

import com.myapp.domain.TestCase;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link TestCase}, with proper type conversions.
 */
@Service
public class TestCaseRowMapper implements BiFunction<Row, String, TestCase> {

    private final ColumnConverter converter;

    public TestCaseRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link TestCase} stored in the database.
     */
    @Override
    public TestCase apply(Row row, String prefix) {
        TestCase entity = new TestCase();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setCaseID(converter.fromRow(row, prefix + "_case_id", Integer.class));
        entity.setName(converter.fromRow(row, prefix + "_name", String.class));
        entity.setSummary(converter.fromRow(row, prefix + "_summary", String.class));
        entity.setStatus(converter.fromRow(row, prefix + "_status", String.class));
        entity.setTestcaseId(converter.fromRow(row, prefix + "_testcase_id", Long.class));
        entity.setUserStoryId(converter.fromRow(row, prefix + "_user_story_id", Long.class));
        return entity;
    }
}
