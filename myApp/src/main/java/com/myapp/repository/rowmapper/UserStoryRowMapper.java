package com.myapp.repository.rowmapper;

import com.myapp.domain.UserStory;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link UserStory}, with proper type conversions.
 */
@Service
public class UserStoryRowMapper implements BiFunction<Row, String, UserStory> {

    private final ColumnConverter converter;

    public UserStoryRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link UserStory} stored in the database.
     */
    @Override
    public UserStory apply(Row row, String prefix) {
        UserStory entity = new UserStory();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setStatus(converter.fromRow(row, prefix + "_status", String.class));
        entity.setDescription(converter.fromRow(row, prefix + "_description", String.class));
        entity.setCaseID(converter.fromRow(row, prefix + "_case_id", Integer.class));
        return entity;
    }
}
