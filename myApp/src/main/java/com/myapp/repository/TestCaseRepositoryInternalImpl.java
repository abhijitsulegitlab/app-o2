package com.myapp.repository;

import static org.springframework.data.relational.core.query.Criteria.where;

import com.myapp.domain.TestCase;
import com.myapp.repository.rowmapper.TestCaseRowMapper;
import com.myapp.repository.rowmapper.UserStoryRowMapper;
import io.r2dbc.spi.Row;
import io.r2dbc.spi.RowMetadata;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.BiFunction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.convert.R2dbcConverter;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.repository.support.SimpleR2dbcRepository;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.sql.Column;
import org.springframework.data.relational.core.sql.Comparison;
import org.springframework.data.relational.core.sql.Condition;
import org.springframework.data.relational.core.sql.Conditions;
import org.springframework.data.relational.core.sql.Expression;
import org.springframework.data.relational.core.sql.Select;
import org.springframework.data.relational.core.sql.SelectBuilder.SelectFromAndJoinCondition;
import org.springframework.data.relational.core.sql.Table;
import org.springframework.data.relational.repository.support.MappingRelationalEntityInformation;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.RowsFetchSpec;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive custom repository implementation for the TestCase entity.
 */
@SuppressWarnings("unused")
class TestCaseRepositoryInternalImpl extends SimpleR2dbcRepository<TestCase, Long> implements TestCaseRepositoryInternal {

    private final DatabaseClient db;
    private final R2dbcEntityTemplate r2dbcEntityTemplate;
    private final EntityManager entityManager;

    private final UserStoryRowMapper userstoryMapper;
    private final TestCaseRowMapper testcaseMapper;

    private static final Table entityTable = Table.aliased("test_case", EntityManager.ENTITY_ALIAS);
    private static final Table testcaseTable = Table.aliased("user_story", "testcase");
    private static final Table userStoryTable = Table.aliased("user_story", "userStory");

    public TestCaseRepositoryInternalImpl(
        R2dbcEntityTemplate template,
        EntityManager entityManager,
        UserStoryRowMapper userstoryMapper,
        TestCaseRowMapper testcaseMapper,
        R2dbcEntityOperations entityOperations,
        R2dbcConverter converter
    ) {
        super(
            new MappingRelationalEntityInformation(converter.getMappingContext().getRequiredPersistentEntity(TestCase.class)),
            entityOperations,
            converter
        );
        this.db = template.getDatabaseClient();
        this.r2dbcEntityTemplate = template;
        this.entityManager = entityManager;
        this.userstoryMapper = userstoryMapper;
        this.testcaseMapper = testcaseMapper;
    }

    @Override
    public Flux<TestCase> findAllBy(Pageable pageable) {
        return createQuery(pageable, null).all();
    }

    RowsFetchSpec<TestCase> createQuery(Pageable pageable, Condition whereClause) {
        List<Expression> columns = TestCaseSqlHelper.getColumns(entityTable, EntityManager.ENTITY_ALIAS);
        columns.addAll(UserStorySqlHelper.getColumns(testcaseTable, "testcase"));
        columns.addAll(UserStorySqlHelper.getColumns(userStoryTable, "userStory"));
        SelectFromAndJoinCondition selectFrom = Select
            .builder()
            .select(columns)
            .from(entityTable)
            .leftOuterJoin(testcaseTable)
            .on(Column.create("testcase_id", entityTable))
            .equals(Column.create("id", testcaseTable))
            .leftOuterJoin(userStoryTable)
            .on(Column.create("user_story_id", entityTable))
            .equals(Column.create("id", userStoryTable));
        // we do not support Criteria here for now as of https://github.com/jhipster/generator-jhipster/issues/18269
        String select = entityManager.createSelect(selectFrom, TestCase.class, pageable, whereClause);
        return db.sql(select).map(this::process);
    }

    @Override
    public Flux<TestCase> findAll() {
        return findAllBy(null);
    }

    @Override
    public Mono<TestCase> findById(Long id) {
        Comparison whereClause = Conditions.isEqual(entityTable.column("id"), Conditions.just(id.toString()));
        return createQuery(null, whereClause).one();
    }

    @Override
    public Mono<TestCase> findOneWithEagerRelationships(Long id) {
        return findById(id);
    }

    @Override
    public Flux<TestCase> findAllWithEagerRelationships() {
        return findAll();
    }

    @Override
    public Flux<TestCase> findAllWithEagerRelationships(Pageable page) {
        return findAllBy(page);
    }

    private TestCase process(Row row, RowMetadata metadata) {
        TestCase entity = testcaseMapper.apply(row, "e");
        entity.setTestcase(userstoryMapper.apply(row, "testcase"));
        entity.setUserStory(userstoryMapper.apply(row, "userStory"));
        return entity;
    }

    @Override
    public <S extends TestCase> Mono<S> save(S entity) {
        return super.save(entity);
    }
}
