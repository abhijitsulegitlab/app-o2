package com.myapp.web.rest;

import com.myapp.domain.UserStory;
import com.myapp.repository.UserStoryRepository;
import com.myapp.service.UserStoryService;
import com.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.myapp.domain.UserStory}.
 */
@RestController
@RequestMapping("/api")
public class UserStoryResource {

    private final Logger log = LoggerFactory.getLogger(UserStoryResource.class);

    private static final String ENTITY_NAME = "userStory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserStoryService userStoryService;

    private final UserStoryRepository userStoryRepository;

    public UserStoryResource(UserStoryService userStoryService, UserStoryRepository userStoryRepository) {
        this.userStoryService = userStoryService;
        this.userStoryRepository = userStoryRepository;
    }

    /**
     * {@code POST  /user-stories} : Create a new userStory.
     *
     * @param userStory the userStory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userStory, or with status {@code 400 (Bad Request)} if the userStory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-stories")
    public Mono<ResponseEntity<UserStory>> createUserStory(@Valid @RequestBody UserStory userStory) throws URISyntaxException {
        log.debug("REST request to save UserStory : {}", userStory);
        if (userStory.getId() != null) {
            throw new BadRequestAlertException("A new userStory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return userStoryService
            .save(userStory)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/user-stories/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /user-stories/:id} : Updates an existing userStory.
     *
     * @param id the id of the userStory to save.
     * @param userStory the userStory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userStory,
     * or with status {@code 400 (Bad Request)} if the userStory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userStory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-stories/{id}")
    public Mono<ResponseEntity<UserStory>> updateUserStory(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UserStory userStory
    ) throws URISyntaxException {
        log.debug("REST request to update UserStory : {}, {}", id, userStory);
        if (userStory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userStory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return userStoryRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return userStoryService
                    .update(userStory)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /user-stories/:id} : Partial updates given fields of an existing userStory, field will ignore if it is null
     *
     * @param id the id of the userStory to save.
     * @param userStory the userStory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userStory,
     * or with status {@code 400 (Bad Request)} if the userStory is not valid,
     * or with status {@code 404 (Not Found)} if the userStory is not found,
     * or with status {@code 500 (Internal Server Error)} if the userStory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-stories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<UserStory>> partialUpdateUserStory(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody UserStory userStory
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserStory partially : {}, {}", id, userStory);
        if (userStory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userStory.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return userStoryRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<UserStory> result = userStoryService.partialUpdate(userStory);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /user-stories} : get all the userStories.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userStories in body.
     */
    @GetMapping("/user-stories")
    public Mono<ResponseEntity<List<UserStory>>> getAllUserStories(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        ServerHttpRequest request
    ) {
        log.debug("REST request to get a page of UserStories");
        return userStoryService
            .countAll()
            .zipWith(userStoryService.findAll(pageable).collectList())
            .map(countWithEntities ->
                ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2())
            );
    }

    /**
     * {@code GET  /user-stories/:id} : get the "id" userStory.
     *
     * @param id the id of the userStory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userStory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-stories/{id}")
    public Mono<ResponseEntity<UserStory>> getUserStory(@PathVariable Long id) {
        log.debug("REST request to get UserStory : {}", id);
        Mono<UserStory> userStory = userStoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userStory);
    }

    /**
     * {@code DELETE  /user-stories/:id} : delete the "id" userStory.
     *
     * @param id the id of the userStory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-stories/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteUserStory(@PathVariable Long id) {
        log.debug("REST request to delete UserStory : {}", id);
        return userStoryService
            .delete(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}
