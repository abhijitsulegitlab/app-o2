package com.myapp.service;

import com.myapp.domain.TestCase;
import com.myapp.repository.TestCaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link TestCase}.
 */
@Service
@Transactional
public class TestCaseService {

    private final Logger log = LoggerFactory.getLogger(TestCaseService.class);

    private final TestCaseRepository testCaseRepository;

    public TestCaseService(TestCaseRepository testCaseRepository) {
        this.testCaseRepository = testCaseRepository;
    }

    /**
     * Save a testCase.
     *
     * @param testCase the entity to save.
     * @return the persisted entity.
     */
    public Mono<TestCase> save(TestCase testCase) {
        log.debug("Request to save TestCase : {}", testCase);
        return testCaseRepository.save(testCase);
    }

    /**
     * Update a testCase.
     *
     * @param testCase the entity to save.
     * @return the persisted entity.
     */
    public Mono<TestCase> update(TestCase testCase) {
        log.debug("Request to save TestCase : {}", testCase);
        return testCaseRepository.save(testCase);
    }

    /**
     * Partially update a testCase.
     *
     * @param testCase the entity to update partially.
     * @return the persisted entity.
     */
    public Mono<TestCase> partialUpdate(TestCase testCase) {
        log.debug("Request to partially update TestCase : {}", testCase);

        return testCaseRepository
            .findById(testCase.getId())
            .map(existingTestCase -> {
                if (testCase.getCaseID() != null) {
                    existingTestCase.setCaseID(testCase.getCaseID());
                }
                if (testCase.getName() != null) {
                    existingTestCase.setName(testCase.getName());
                }
                if (testCase.getSummary() != null) {
                    existingTestCase.setSummary(testCase.getSummary());
                }
                if (testCase.getStatus() != null) {
                    existingTestCase.setStatus(testCase.getStatus());
                }

                return existingTestCase;
            })
            .flatMap(testCaseRepository::save);
    }

    /**
     * Get all the testCases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Flux<TestCase> findAll(Pageable pageable) {
        log.debug("Request to get all TestCases");
        return testCaseRepository.findAllBy(pageable);
    }

    /**
     * Get all the testCases with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Flux<TestCase> findAllWithEagerRelationships(Pageable pageable) {
        return testCaseRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Returns the number of testCases available.
     * @return the number of entities in the database.
     *
     */
    public Mono<Long> countAll() {
        return testCaseRepository.count();
    }

    /**
     * Get one testCase by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Mono<TestCase> findOne(Long id) {
        log.debug("Request to get TestCase : {}", id);
        return testCaseRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the testCase by id.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    public Mono<Void> delete(Long id) {
        log.debug("Request to delete TestCase : {}", id);
        return testCaseRepository.deleteById(id);
    }
}
