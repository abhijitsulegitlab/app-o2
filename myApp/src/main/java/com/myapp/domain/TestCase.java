package com.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A TestCase.
 */
@Table("test_case")
public class TestCase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @NotNull(message = "must not be null")
    @Column("case_id")
    private Integer caseID;

    @NotNull(message = "must not be null")
    @Column("name")
    private String name;

    @Column("summary")
    private String summary;

    @NotNull(message = "must not be null")
    @Column("status")
    private String status;

    @Transient
    @JsonIgnoreProperties(value = { "userstories" }, allowSetters = true)
    private UserStory testcase;

    @Transient
    @JsonIgnoreProperties(value = { "userstories" }, allowSetters = true)
    private UserStory userStory;

    @Column("testcase_id")
    private Long testcaseId;

    @Column("user_story_id")
    private Long userStoryId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TestCase id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCaseID() {
        return this.caseID;
    }

    public TestCase caseID(Integer caseID) {
        this.setCaseID(caseID);
        return this;
    }

    public void setCaseID(Integer caseID) {
        this.caseID = caseID;
    }

    public String getName() {
        return this.name;
    }

    public TestCase name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return this.summary;
    }

    public TestCase summary(String summary) {
        this.setSummary(summary);
        return this;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return this.status;
    }

    public TestCase status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserStory getTestcase() {
        return this.testcase;
    }

    public void setTestcase(UserStory userStory) {
        this.testcase = userStory;
        this.testcaseId = userStory != null ? userStory.getId() : null;
    }

    public TestCase testcase(UserStory userStory) {
        this.setTestcase(userStory);
        return this;
    }

    public UserStory getUserStory() {
        return this.userStory;
    }

    public void setUserStory(UserStory userStory) {
        this.userStory = userStory;
        this.userStoryId = userStory != null ? userStory.getId() : null;
    }

    public TestCase userStory(UserStory userStory) {
        this.setUserStory(userStory);
        return this;
    }

    public Long getTestcaseId() {
        return this.testcaseId;
    }

    public void setTestcaseId(Long userStory) {
        this.testcaseId = userStory;
    }

    public Long getUserStoryId() {
        return this.userStoryId;
    }

    public void setUserStoryId(Long userStory) {
        this.userStoryId = userStory;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TestCase)) {
            return false;
        }
        return id != null && id.equals(((TestCase) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TestCase{" +
            "id=" + getId() +
            ", caseID=" + getCaseID() +
            ", name='" + getName() + "'" +
            ", summary='" + getSummary() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
