package com.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A UserStory.
 */
@Entity
@Table(name = "user_story")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserStory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "status", nullable = false)
    private String status;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "case_id")
    private Integer caseID;

    @OneToMany(mappedBy = "userStory")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "testcase", "userStory" }, allowSetters = true)
    private Set<TestCase> userstories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public UserStory id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return this.status;
    }

    public UserStory status(String status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return this.description;
    }

    public UserStory description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCaseID() {
        return this.caseID;
    }

    public UserStory caseID(Integer caseID) {
        this.setCaseID(caseID);
        return this;
    }

    public void setCaseID(Integer caseID) {
        this.caseID = caseID;
    }

    public Set<TestCase> getUserstories() {
        return this.userstories;
    }

    public void setUserstories(Set<TestCase> testCases) {
        if (this.userstories != null) {
            this.userstories.forEach(i -> i.setUserStory(null));
        }
        if (testCases != null) {
            testCases.forEach(i -> i.setUserStory(this));
        }
        this.userstories = testCases;
    }

    public UserStory userstories(Set<TestCase> testCases) {
        this.setUserstories(testCases);
        return this;
    }

    public UserStory addUserstory(TestCase testCase) {
        this.userstories.add(testCase);
        testCase.setUserStory(this);
        return this;
    }

    public UserStory removeUserstory(TestCase testCase) {
        this.userstories.remove(testCase);
        testCase.setUserStory(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserStory)) {
            return false;
        }
        return id != null && id.equals(((UserStory) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "UserStory{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", description='" + getDescription() + "'" +
            ", caseID=" + getCaseID() +
            "}";
    }
}
