package com.myapp.repository;

import com.myapp.domain.TestCase;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the TestCase entity.
 */
@Repository
public interface TestCaseRepository extends JpaRepository<TestCase, Long> {
    default Optional<TestCase> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<TestCase> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<TestCase> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct testCase from TestCase testCase left join fetch testCase.testcase",
        countQuery = "select count(distinct testCase) from TestCase testCase"
    )
    Page<TestCase> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct testCase from TestCase testCase left join fetch testCase.testcase")
    List<TestCase> findAllWithToOneRelationships();

    @Query("select testCase from TestCase testCase left join fetch testCase.testcase where testCase.id =:id")
    Optional<TestCase> findOneWithToOneRelationships(@Param("id") Long id);
}
