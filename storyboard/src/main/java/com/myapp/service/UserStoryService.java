package com.myapp.service;

import com.myapp.domain.UserStory;
import com.myapp.repository.UserStoryRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserStory}.
 */
@Service
@Transactional
public class UserStoryService {

    private final Logger log = LoggerFactory.getLogger(UserStoryService.class);

    private final UserStoryRepository userStoryRepository;

    public UserStoryService(UserStoryRepository userStoryRepository) {
        this.userStoryRepository = userStoryRepository;
    }

    /**
     * Save a userStory.
     *
     * @param userStory the entity to save.
     * @return the persisted entity.
     */
    public UserStory save(UserStory userStory) {
        log.debug("Request to save UserStory : {}", userStory);
        return userStoryRepository.save(userStory);
    }

    /**
     * Update a userStory.
     *
     * @param userStory the entity to save.
     * @return the persisted entity.
     */
    public UserStory update(UserStory userStory) {
        log.debug("Request to save UserStory : {}", userStory);
        return userStoryRepository.save(userStory);
    }

    /**
     * Partially update a userStory.
     *
     * @param userStory the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<UserStory> partialUpdate(UserStory userStory) {
        log.debug("Request to partially update UserStory : {}", userStory);

        return userStoryRepository
            .findById(userStory.getId())
            .map(existingUserStory -> {
                if (userStory.getStatus() != null) {
                    existingUserStory.setStatus(userStory.getStatus());
                }
                if (userStory.getDescription() != null) {
                    existingUserStory.setDescription(userStory.getDescription());
                }
                if (userStory.getCaseID() != null) {
                    existingUserStory.setCaseID(userStory.getCaseID());
                }

                return existingUserStory;
            })
            .map(userStoryRepository::save);
    }

    /**
     * Get all the userStories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<UserStory> findAll(Pageable pageable) {
        log.debug("Request to get all UserStories");
        return userStoryRepository.findAll(pageable);
    }

    /**
     * Get one userStory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UserStory> findOne(Long id) {
        log.debug("Request to get UserStory : {}", id);
        return userStoryRepository.findById(id);
    }

    /**
     * Delete the userStory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete UserStory : {}", id);
        userStoryRepository.deleteById(id);
    }
}
